﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblCompteur = New System.Windows.Forms.Label()
        Me.btnPlus = New System.Windows.Forms.Button()
        Me.btnRaz = New System.Windows.Forms.Button()
        Me.btnMoins = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblCompteur
        '
        Me.lblCompteur.AccessibleDescription = ""
        Me.lblCompteur.AccessibleName = "lblCompteur"
        Me.lblCompteur.AutoSize = True
        Me.lblCompteur.Location = New System.Drawing.Point(379, 156)
        Me.lblCompteur.Name = "lblCompteur"
        Me.lblCompteur.Size = New System.Drawing.Size(13, 13)
        Me.lblCompteur.TabIndex = 0
        Me.lblCompteur.Text = "0"
        '
        'btnPlus
        '
        Me.btnPlus.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnPlus.Location = New System.Drawing.Point(491, 151)
        Me.btnPlus.Name = "btnPlus"
        Me.btnPlus.Size = New System.Drawing.Size(75, 23)
        Me.btnPlus.TabIndex = 1
        Me.btnPlus.Text = "+"
        Me.btnPlus.UseVisualStyleBackColor = True
        '
        'btnRaz
        '
        Me.btnRaz.Location = New System.Drawing.Point(363, 214)
        Me.btnRaz.Name = "btnRaz"
        Me.btnRaz.Size = New System.Drawing.Size(75, 23)
        Me.btnRaz.TabIndex = 2
        Me.btnRaz.Text = "RAZ"
        Me.btnRaz.UseVisualStyleBackColor = True
        '
        'btnMoins
        '
        Me.btnMoins.Location = New System.Drawing.Point(198, 152)
        Me.btnMoins.Name = "btnMoins"
        Me.btnMoins.Size = New System.Drawing.Size(75, 23)
        Me.btnMoins.TabIndex = 3
        Me.btnMoins.Text = "-"
        Me.btnMoins.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btnMoins)
        Me.Controls.Add(Me.btnRaz)
        Me.Controls.Add(Me.btnPlus)
        Me.Controls.Add(Me.lblCompteur)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblCompteur As Label
    Friend WithEvents btnPlus As Button
    Friend WithEvents btnRaz As Button
    Friend WithEvents btnMoins As Button
End Class
