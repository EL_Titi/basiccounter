﻿Imports System
Imports BasicCounterClassLibrary


Public Class Form1
    Dim counter As Compteur()
    counter = Compteur.New()

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub lblCompteur_Click(sender As Object, e As EventArgs) Handles lblCompteur.Click
        lblCompteur.Text = counter.get()
    End Sub

    Private Sub btnPlus_Click(sender As Object, e As EventArgs) Handles btnPlus.Click
        counter.increment()
        lblCompteur.Text = counter.get()
    End Sub

    Private Sub btnMoins_Click(sender As Object, e As EventArgs) Handles btnMoins.Click
        counter.decrement()
        lblCompteur.Text = counter.get()
    End Sub

    Private Sub btnRaz_Click(sender As Object, e As EventArgs) Handles btnRaz.Click
        counter.reinit()
        lblCompteur.Text = counter.get()
    End Sub
End Class
