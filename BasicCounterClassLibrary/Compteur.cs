﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterClassLibrary
{
    public class Compteur
    {
        private
        int nombre;

        public

        Compteur()
        {
            this.nombre = 0;
        }

        int GetNombre()
        {
            return nombre;
        }

        void    increment()
        {
            nombre++;
        }

        void    decrement()
        {
            nombre--;
        }

        void    reinit()
        {
            nombre = 0;
        }
    }
}
